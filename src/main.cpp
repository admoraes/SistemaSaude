#include "../inc/paciente.hpp"
#include "../inc/medico.hpp"
#include "../inc/acompanhante.hpp"

#include <iostream>
#include <string>

using namespace std;

int main()
{
	int opcao;

	cout << "====MENU DE CADASTRO====" << endl;
	cout << "1 - Cadastrar Medico" << endl;
	cout << "2 - Cadastrar Paciente" << endl;
	cout << "3 - Cadastrar Acompanhante" << endl;
	cout << "Selecione a opcao desejada: ";
	cin >> opcao;

	return 0;
}
