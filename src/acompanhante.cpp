#include "../inc/acompanhante.hpp"

Acompanhante::Acompanhante()
{
}

Acompanhante::~Acompanhante()
{
}

void Acompanhante::setTelefone(string telefone)
{
	this->telefone = telefone;
}

void Acompanhante::setEndereco(string endereco)
{
	this->endereco = endereco;
}

string Acompanhante::getTelefone()
{
	return this->telefone;
}

string Acompanhante::getEndereco()
{
	return this->endereco;
}
