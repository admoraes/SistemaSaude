#include "../inc/pessoa.hpp"

Pessoa::Pessoa()
{
}

Pessoa::~Pessoa()
{
}

void Pessoa::setNome(string nome)
{
	this->nome = nome;
}

void Pessoa::setCpf(string cpf)
{
	this->cpf = cpf;
}

void Pessoa::setIdade(int idade)
{
	this->idade = idade;
}

string Pessoa::getNome()
{
	return this->nome;
}

string Pessoa::getCpf()
{
	return this->cpf;
}

int Pessoa::getIdade()
{
	return this->idade;
}
