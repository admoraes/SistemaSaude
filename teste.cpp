#include "inc/acompanhante.hpp"

#include <iostream>
#include <string>

using namespace std;

int main()
{
	Acompanhante acompanhante1;

	acompanhante1.setNome("Fulano da Silva");
	acompanhante1.setCpf("000000000");
	acompanhante1.setIdade(50);
	acompanhante1.setTelefone("99999999");
	acompanhante1.setEndereco("Avenida Santa Maria");

	cout << acompanhante1.getNome() << endl;
	cout << acompanhante1.getCpf() << endl;
	cout << acompanhante1.getIdade() << endl;
	cout << acompanhante1.getTelefone() << endl;
	cout << acompanhante1.getEndereco() << endl;

	return 0;
}
