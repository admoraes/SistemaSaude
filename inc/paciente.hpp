#ifndef PACIENTE_HPP
#define PACIENTE_HPP

#include "pessoa.hpp"

#include <string>

using namespace std;

class Paciente : public Pessoa
{
	private:
		string diagnostico;

	public:
		Paciente();
		~Paciente();
		void setDiagnostico(string diagnostico);
		string getDiagnostico();
};

#endif
