#ifndef MEDICO_HPP
#define MEDICO_HPP

#include "pessoa.hpp"

#include <string>

using namespace std;

class Medico : public Pessoa
{
	private:
		string especialidade;
	public:
		Medico();
		~Medico();
		void setEspecialidade(string especialidade);
		string getEspecialidade();
};

#endif
