#ifndef ACOMPANHANTE_HPP
#define ACOMPANHANTE_HPP

#include "pessoa.hpp"

#include <string>

using namespace std;

class Acompanhante : public Pessoa
{
	private:
		string telefone, endereco;

	public:
		Acompanhante();
		~Acompanhante();

		void setTelefone(string telefone);
		void setEndereco(string endereco);

		string getTelefone();
		string getEndereco();
};

#endif
