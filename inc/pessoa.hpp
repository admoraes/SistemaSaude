#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <string>

using namespace std;

class Pessoa
{
	private:
		string nome, cpf;
		int idade;

	public:
		Pessoa();
		~Pessoa();

		void setNome(string nome);
		void setCpf(string cpf);
		void setIdade(int idade);

		string getNome();
		string getCpf();
		int getIdade();
};

#endif
